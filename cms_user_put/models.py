from django.db import models

# Create your models here.


class Contenido(models.Model): #clave primaria de contenidos --> identificador
    clave = models.CharField(max_length=64)  # Recurso
    valor = models.TextField()  # HTML
    #comentario = models.TextField() --> no es muy óptimo

    def __str__(self):
        return str(self.id) + ": " + self.clave


class Comentario(models.Model):
    contenido = models.ForeignKey(Contenido, on_delete=models.CASCADE) #foregnkey --> clave externa el
    # on_delete lo que hace es que cuando te cargues informacion de contenido, cargate todas las entradas de comentario de ese contenido.
    titulo = models.CharField(max_length=200)
    comentario = models.TextField(blank=False)
    fecha = models.DateTimeField('publicado')

    def __str__(self):
        return str(self.id) + ": " + self.titulo+ "---"+ self.contenido.clave

    def validacion(self):
        return("Cuerpo" in self.comentario) #para que cargue el shell control d

"""contenido es una tabla valor, clave que se relaciona con la tabla comentarios: 1 a 1 , 1 a todos, todos a 1 ,
 todos a todos"""
"""Nos quedamos con uno a mucho, created super user

python manage.py sehll, from .models import conetnido, comentario
contenido.objetc.all()[1]
contenido.objects.get(recurso=clave)
get --> lo normal es que me devuelva un único recurso y excepcion 
filter --> queryset con una lista y me deuvelve una lista vacia si no existiera el recurso
contenido.object.filter(clave--startwith="Recurso")
c = Contenido(clave = "Recurso", valor = "Este es el recurso 8")
c.save()
from django.utils import timezone
c.comentario_set.create(titulo="COMENTARIO DE RECURSO 8", contenido ="Este es el cuerpo del conenido", fecha =timezone.now())
c = comentario.objects.get(id=2)
c.validation() --> me tiee que devolver true

tinyual
la acortacion no puede ser aleatoria sino que añadimos una entrada en el formulario 
si no se pone nada ahi por defecto le asgnará el 1  de forma secuencial
se guarda en un fichero de datos
"""