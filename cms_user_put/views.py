from django.shortcuts import render, redirect
from django.http import HttpResponse, Http404
from .models import Contenido
from django.template import loader #importar el cargador de templates
from django.views.decorators.csrf import csrf_exempt  # Saltar mecanismo de seguridad CSRF
from django.contrib.auth import logout
# Create your views here.

def index(request):
	content_list = Contenido.objects.all()
	#cargar el template
	template = loader.get_template('cms_user_put/index.html')
	#crear el contexto del template
	contexto = {
		'content_list': content_list,

	}
	#renderizarlo y devolverlo
	return HttpResponse(template.render(contexto, request))

formulario = """
<form accion="" method="POST"
 """

@csrf_exempt  # Me salto mecanismo de seguridad
def get_resource(request, recurso):

	#PUT O POST
	if request.method in ["PUT", "POST"]:
		if request.method == "POST":
			valor = request.POST["valor"]
		else:
			valor = request.body.decode()

	#compruebo que el contenido no está en la base de datos (solo PUT)
		#PUT
		try:
			contenido = Contenido.objects.get(clave=recurso)
			contenido.valor = valor
			contenido.save()
		except Contenido.DoesNotExist:
			if request.user.is_authenticated:
				contenido = Contenido.objects.get(clave=recurso)
				contenido.save()
				respuesta = "Contenido guardado"
			else:
				respuesta = "No estas autenticado, <a href=/login>autenticate</a>"
			return HttpResponse(respuesta)


	# GET
	try:
		contenido = Contenido.objects.get(clave=recurso)
		respuesta = HttpResponse(HttpResponse("El recurso pedido es: " + contenido.clave + ". Su valor es: " + contenido.valor + ". Su identificador es: " + str(contenido.id)))
	except Contenido.DoesNotExist:
		if request.user.is_authenticated:
			respuesta = HttpResponse(formulario)
		else:
			respuesta = HttpResponse("Para poder dar de alta un recurso tienes que estar <a href=/login/>autenticado</a>")
	return respuesta

def loggedIn(request):
	if request.user.is_authenticated:
		respuesta = "Bienvenido "+request.user.username + " al CMS de SARO"
	else:
		respuesta = "No estas autenticado, <a href=/login>autenticate</a>"
	return HttpResponse(respuesta)

#para autenticarse irás a admin, entras con tu user y contraseña, add user, nombre y password(tiene restricciones), D4v1ACMSI,
#permisos --> activate (puede o no acceder a los servicios), staff (utilizar los logs--admin), superuser (todocontrolado), groups (profesores, alumnos),
#
#created useruser con clave

def logout_vista(request):
	logout(request)
	return redirect("/login")

def imagen(request):
	template =loader.get_template("cms_user_put/plantilla.html")
	context = {}
	return HttpResponse(template.render(context, request))