from django.urls import path
from . import views

urlpatterns = [
	path('', views.index),
	path('acceso/', views.loggedIn),
	path('logout/', views.logout_vista),
	path('imagen', views.imagen),
	path('<str:recurso>/', views.get_resource),
]
#ES IMPORTANTE SEGUIR EL ORDEN DE LOS RECURSOS EN FUNCION DE LA IMPORTANCIA. SI EL RECURSO ES ACCESO ENTRA EN EL SEGUNDO,
#PERO SI PONEMOS ANTES EL TERCERO, ENTRARÁ AHÍ

